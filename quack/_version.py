# credits to: https://stackoverflow.com/a/2073599
import pkg_resources

_version = pkg_resources.require('quack')[0].version

__all__ = '_version'
