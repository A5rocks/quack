import hikari

from quack import MessageEvent

HikariMessageEvent = MessageEvent[hikari.models.messages.Message]

__all__ = 'HikariMessageEvent'
