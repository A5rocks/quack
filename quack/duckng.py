import typing
import importlib
import logging

from .features import FeatureMarker
from .command_trie import CommandTrie
from .prefix_trie import PrefixTrie
from .event import MessageProto
from .command import QuackCommand

T = typing.TypeVar('T', bound=MessageProto)
callbackT = typing.TypeVar('callbackT', bound=MessageProto, contravariant=True)


class PrefixCallback(typing.Protocol[callbackT]):
    async def __call__(self, __message: callbackT) -> typing.List[str]:
        ...


class Duck(typing.Generic[T]):
    features: typing.Dict[str, typing.Type[FeatureMarker]]
    prefixes: PrefixTrie
    prefix_callback: typing.Optional[PrefixCallback[T]]
    commands: CommandTrie[T]
    allow_spaces: bool
    logger: logging.Logger
    command_seperator: str

    def __init__(
            self,
            prefixes: typing.Optional[typing.List[str]] = None,
            prefix_callback: typing.Optional[PrefixCallback[T]] = None,
            allow_spaces: bool = False,
            command_seperator: str = ' '
    ) -> None:
        self.features = {}
        self.prefixes = PrefixTrie()
        self.commands = CommandTrie()
        self.allow_spaces = allow_spaces
        self.prefix_callback = prefix_callback
        self.logger = logging.getLogger('quack')
        self.command_seperator = command_seperator

        for prefix in prefixes or []:
            self.prefixes.add_prefix(prefix)

    async def _custom_prefixes(self, message: T) -> typing.List[str]:
        if self.prefix_callback is None:
            return []

        return await self.prefix_callback(message)

    def load_feature(self, path: str) -> typing.Optional[typing.Type[FeatureMarker]]:
        self.logger.info('loading quack feature: %s', path)
        if ':' not in path:
            logging.warning('Invalid path, should contain `:` followed by class name: %s', path)
            return

        if self.features.get(path) is not None:
            logging.warning('Invalid path, has been registered before: %s', path)
            return

        filepath, klasspath = path.rsplit(':', 1)

        mod = importlib.import_module(filepath)
        feat = getattr(mod, klasspath)

        if not issubclass(feat, FeatureMarker):
            logging.warning('Invalid feature, does not subclass `FeatureMarker` at: %s', path)
            return

        self.features[path] = feat
        self.process_addition(feat)
        return feat

    def remove_feature(self, path: str) -> None:
        self.logger.info('removing quack feature: %s', path)
        if path not in self.features:
            logging.warning('Trying to remove a feature that is added: %s', path)
            return

        feat = self.features[path]
        self.process_removal(feat)
        del feat

    def process_addition(self, feature: typing.Type[FeatureMarker]) -> None:
        feat_info_s = getattr(feature, '_f_info', None)
        if feat_info_s is None:
            raise UserWarning('What kinda magic are you trying to pull here >:(')

        for feat_info in feat_info_s:
            if feat_info.type == 'command':
                self.add_command(feat_info.info)

    def process_removal(self, feature: typing.Type[FeatureMarker]) -> None:
        feat_info_s = getattr(feature, '_f_info', None)
        for feat_info in feat_info_s:
            if feat_info.type == 'command':
                self.remove_command(feat_info.info)

    def add_command(self, command: QuackCommand[T]) -> None:
        self.logger.debug('loading command: %r', command)
        self.commands.add_command(command.name.split(self.command_seperator), command)

        for alias in command.aliases:
            self.commands.add_command(alias.split(self.command_seperator), command)

    def remove_command(self, command: QuackCommand[T]) -> None:
        self.logger.debug('removing command: %r', command)
        self.commands.remove_specific_command(command.name.split(self.command_seperator), command)

        for alias in command.aliases:
            self.commands.remove_specific_command(alias.split(self.command_seperator), command)
