import collections
import functools
import inspect
import logging
import typing

from .converters import ConverterProto, get_converter
from .event import MessageEvent, MessageProto
from .executor import AnyIOResultor

T = typing.TypeVar('T', bound=MessageProto)

# TODO: when PEP612 is implemented, use that for decorators.


# https://github.com/python/mypy/issues/708#issuecomment-647124281
class Command(typing.Protocol[T]):
    async def __call__(self, __event: MessageEvent[T]) -> None:
        ...


# TODO: hmmm, not sure how to make this one work out... (*args to repr whatever)
class ConverterCommand(typing.Protocol[T]):
    async def __call__(self, __event: MessageEvent[T], *__args: typing.Any) -> None:
        ...


_ConverterCommand = typing.Callable[..., typing.Awaitable[None]]
MiddlewareCallable = typing.Callable[..., typing.Awaitable[MessageEvent[T]]]
Check = typing.Callable[..., typing.Awaitable[bool]]


class QuackCommand(typing.Generic[T]):
    __slots__ = ('callback', 'name', 'aliases', 'after_fail', 'metadata', 'metadata_lens',
                 'middleware', 'checks')

    callback: Command[T]
    name: str
    aliases: typing.Set[str]
    metadata: typing.MutableMapping[object, typing.Set[typing.Any]]
    metadata_lens: typing.MutableMapping[object, int]
    after_fail: typing.Set['QuackCommand[T]']
    middleware: typing.List[MiddlewareCallable[T]]
    checks: typing.Set[Check]

    def __init__(
            self,
            callback: Command[T],
            name: str,
            aliases: typing.List[str],
            after_fail: typing.Set['QuackCommand[T]']
    ) -> None:
        self.callback = callback
        self.name = name
        self.aliases = set(aliases)
        self.metadata = collections.defaultdict(set)
        self.metadata_lens = collections.defaultdict(lambda: -1)
        self.after_fail = after_fail
        self.checks = set()
        self.middleware = []

    async def __call__(self, event: MessageEvent[T]) -> None:
        bool_resultor = AnyIOResultor[bool]

        async with bool_resultor() as tg:
            for check in self.checks:
                await tg.spawn(check, event)

        if False in tg.results:
            return

        if any(isinstance(obj, BaseException) for obj in tg.results):
            for exc in filter(lambda obj: isinstance(obj, BaseException), tg.results):
                logging.error('an error was raised in a cheap check!', exc_info=exc)

            return

        for middleware in self.middleware:
            event = await middleware(event)

        return await self.callback(event)

    def add_check(self, check: Check) -> None:
        self.checks |= {check}

    def add_middleware(self, middleware: MiddlewareCallable[T]) -> None:
        self.middleware = [middleware] + self.middleware

    def __repr__(self) -> str:
        return (
            f'<QuackCommand name={self.name!r} callback={self.name!r} aliases={self.aliases!r} with '
            f'{len(self.after_fail)!r} dependencies, {len(self.metadata)!r} pieces of metadata, '
            f'{len(self.middleware)!r} middleware, and {len(self.checks)!r} checks>'
        )


def simple_command(
        name: typing.Optional[str] = None,
        aliases: typing.Optional[typing.List[str]] = None,
        after_fail: typing.Optional[typing.List[QuackCommand[T]]] = None
        # TODO: the return typing for this is deceiving, but is needed due to mypy... any other way?
) -> typing.Callable[..., typing.Any]:
    def wrapped_present(coro: Command[T]) -> QuackCommand[T]:
        return QuackCommand(
            coro,
            name=name or getattr(coro, '__name__'),
            aliases=aliases or [],
            after_fail=set(after_fail or [])
        )

    return wrapped_present


def command(
        name: typing.Optional[str] = None,
        aliases: typing.Optional[typing.List[str]] = None,
        after_fail: typing.Optional[typing.List[QuackCommand[T]]] = None
) -> typing.Callable[[_ConverterCommand], QuackCommand[T]]:
    # TODO: fix ConverterCommand to work with this
    def wrapped_present(coro_: _ConverterCommand) -> QuackCommand[T]:
        coro = typing.cast(ConverterCommand[T], coro_)
        typed_params = typing.get_type_hints(coro)
        sig = inspect.signature(coro)
        sig_params = sig.parameters
        real_params = list(sig.parameters.keys())[1:]
        params = {k: v for k, v in typed_params.items() if k in real_params}

        assert real_params == list(params.keys())

        last = None
        converters: typing.List[ConverterProto[object]] = []

        for k, param in reversed(params.items()):
            last = get_converter(
                typing.cast(typing.Type[T], param),
                sig_params[k].default if sig_params[k].default != sig.empty else None,
                last
            )
            converters.append(last)

        @functools.wraps(coro)
        async def new_command_(event: MessageEvent[typing.Any]) -> None:
            args = event.args
            async_fun_args: typing.List[object] = []

            for converter in converters:
                if not event.duck.allow_spaces:
                    while args and not args[0]:
                        args = args[1:]

                args, res = await converter.convert(args)
                async_fun_args.append(res)

            if args:
                return

            await coro(event, *async_fun_args)

        # fricking functools.wrap squashing types...
        new_command = typing.cast(Command[T], new_command_)

        return QuackCommand(
            new_command,
            name=name or getattr(coro, '__name__'),
            aliases=aliases or [],
            after_fail=set(after_fail or [])
        )

    return wrapped_present


def meta(
        metadata_name: typing.Any,
        contents_: typing.Union[typing.Any, typing.Set[typing.Any]],
        length_restriction: int = -1
) -> typing.Callable[[QuackCommand[T]], QuackCommand[T]]:
    if not isinstance(contents_, set):
        contents = {contents_}
    else:
        contents = contents_

    def wrapped_present(editing_command: QuackCommand[T]) -> QuackCommand[T]:
        if (editing_command.metadata_lens[metadata_name] != -1
                and length_restriction != -1
                and editing_command.metadata_lens[metadata_name] != length_restriction):
            # TODO: more descriptive error
            raise UserWarning('bad `length_restriction`.')

        if length_restriction != -1:
            editing_command.metadata_lens[metadata_name] = length_restriction

        editing_command.metadata[metadata_name] |= contents

        if (length_restriction != -1
                and len(editing_command.metadata[metadata_name]) > editing_command.metadata_lens[metadata_name]):
            # TODO: more descriptive error
            raise UserWarning('more than `length_restriction` things in metadata set.')

        return editing_command

    return wrapped_present
