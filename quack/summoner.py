# Gets your commands from other files loaded.
import importlib.machinery
import importlib.util
import types
import typing

from .command import QuackCommand
from .duck import Duck
from .event import MessageProto

T = typing.TypeVar('T', bound=MessageProto)


def summon(inst: Duck[T], filename: str, name: str = 'summon') -> types.ModuleType:
    # acknowledgements: https://stackoverflow.com/a/19011259
    loader = importlib.machinery.SourceFileLoader(name, filename)
    spec = importlib.util.spec_from_loader(loader.name, loader)
    module = importlib.util.module_from_spec(spec)
    loader.exec_module(module)

    # now `module` is the summon we want to drop into the ... DUCK.
    inst.add_module(filename, module)

    return module


def dispel(inst: Duck[T], module: types.ModuleType) -> None:
    for name in dir(module):
        value = getattr(module, name)

        if isinstance(value, QuackCommand):
            inst.remove_specific_command(value.name, value)


def resummon(inst: Duck[T], filename: str, name: str = 'summon') -> types.ModuleType:
    dispel(inst, inst.modules[filename])
    return summon(inst, filename, name)
