import typing
from .command import Command, QuackCommand

T = typing.TypeVar('T')


class FeatureMeta(type):
    def __new__(cls, clsname,  bases, attrs, generic_feature=False):
        # take care of any race conditions
        if generic_feature:
            # juuuust in case
            attrs['_f_info'] = set()

            return super().__new__(cls, clsname, bases, attrs)

        feature_bases = [base for base in bases if issubclass(base, FeatureMarker) and base != FeatureMarker]

        # we aren't going to inherit from the feature bases lol
        bases = [base for base in bases if base not in feature_bases and base != FeatureMarker] + [FeatureMarker]

        # we want to scrape out any marked pieces of information
        scraped_attrs = {v for v in attrs.values() if isinstance(v, FeatureInfo)}
        attrs = {k: v for k, v in attrs.items() if v not in scraped_attrs}

        for feature_base in feature_bases:
            scraped_attrs.update(feature_base._f_info)

        if '_f_info' in attrs:
            raise UserWarning('The attribute `_f_info` is reserved for `quack` on features :P')

        attrs['_f_info'] = scraped_attrs

        return super().__new__(cls, clsname, tuple(bases), attrs)


class FeatureInfo(typing.Generic[T]):
    __slots__ = ('type', 'info')
    type: str
    info: T

    def __init__(self, typ, information):
        self.type = typ
        self.info = information


class FeatureMarker(metaclass=FeatureMeta, generic_feature=True):
    ...


class Feature(FeatureMarker, generic_feature=True):
    @staticmethod
    def simple_command(
            name: typing.Optional[str] = None,
            aliases: typing.Optional[typing.List[str]] = None,
            after_fail: typing.Optional[typing.List[FeatureInfo[QuackCommand[T]]]] = None
    ) -> typing.Callable[[Command[T]], FeatureInfo[QuackCommand[T]]]:

        def wrapped(async_fun: Command[T]) -> FeatureInfo[QuackCommand[T]]:
            # scoping weirdness, check out https://bugs.python.org/issue37568.
            # tl;dr assignments in a function body turns the var into a local,
            # which means it won't find the param above.
            nonlocal name
            nonlocal aliases
            nonlocal after_fail

            if name is None:
                name = getattr(async_fun, '__name__', None)
                assert name is not None

            if aliases is None:
                aliases = []

            if after_fail is None:
                after_fail = set()
            else:
                after_fail = set(after_fail)

            return FeatureInfo('command', QuackCommand(
                name=name,
                callback=async_fun,
                aliases=aliases,
                after_fail=after_fail
            ))

        return wrapped
