import typing

T = typing.TypeVar('T')

# NOTE: this is more a task for `typing.Annotation`... but that's 3.9+

if typing.TYPE_CHECKING:
    MinimumList = typing.List[T]
else:
    class MinimumList(typing.Generic[T]):
        ...
