import abc
import typing

if typing.TYPE_CHECKING:
    from .command import QuackCommand
    from .duck import Duck
else:
    Duck = typing.Union
    QuackCommand = typing.Union


class MessageProto(typing.Protocol):
    # a read-only property
    @property
    @abc.abstractmethod
    def content(self) -> typing.Optional[str]:
        ...


T = typing.TypeVar('T', bound=MessageProto)


class OriginEvent(typing.Protocol[T]):
    message: T


class MessageEvent(typing.Generic[T]):
    __slots__ = ('command', 'prefix', 'message', 'duck', 'errors', 'args', 'meta')
    prefix: str
    command: str
    message: T
    errors: typing.MutableMapping[QuackCommand[T], typing.Optional[BaseException]]
    duck: Duck[T]
    args: typing.List[str]

    # middlewares will normally pass results into here...
    meta: typing.Dict[str, typing.Any]

    def __init__(
            self,
            prefix: str,
            command: str,
            message: T,
            errors: typing.MutableMapping[QuackCommand[T], typing.Optional[BaseException]],
            duck: Duck[T],
            args: typing.List[str]
    ):
        self.command = command
        self.prefix = prefix
        self.message = message
        self.duck = duck
        self.errors = errors
        self.args = args
        self.meta = {}
