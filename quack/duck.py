# the main interface
import collections
import logging
import types
import typing

from .command import QuackCommand
from .command_trie import CommandTrie
from .converters import ConverterException
from .event import MessageEvent, MessageProto
from .executor import AnyIOExecutor
from .prefix_trie import PrefixTrie

T = typing.TypeVar('T', bound=MessageProto)
callbackT = typing.TypeVar('callbackT', bound=MessageProto, contravariant=True)


class PrefixCallback(typing.Protocol[callbackT]):
    async def __call__(self, __message: callbackT) -> typing.List[str]:
        ...


class Duck(typing.Generic[T]):
    prefixes: PrefixTrie
    prefix_callback: typing.Optional[PrefixCallback[T]]
    commands: CommandTrie[T]
    modules: typing.MutableMapping[str, types.ModuleType]
    command_list: typing.Set[str]
    alias_storage: typing.MutableMapping[str, typing.Set[str]]
    metadata: typing.MutableMapping[str, typing.MutableMapping[typing.Any, typing.Set[typing.Any]]]
    metadata_lens: typing.MutableMapping[str, typing.MutableMapping[typing.Any, int]]
    allow_spaces: bool
    logger: logging.Logger

    def __init__(
            self,
            prefixes: typing.Optional[typing.List[str]] = None,
            prefix_callback: typing.Optional[PrefixCallback[T]] = None,
            allow_spaces: bool = False
    ) -> None:
        self.prefixes = PrefixTrie()
        self.commands = CommandTrie()
        self.modules = {}
        self.command_list = set()
        self.alias_storage = {}
        self.metadata = collections.defaultdict(lambda: collections.defaultdict(set))
        self.metadata_lens = collections.defaultdict(lambda: collections.defaultdict(lambda: -1))
        self.prefix_callback = prefix_callback
        self.allow_spaces = allow_spaces
        self.logger = logging.getLogger('quack')

        for prefix in prefixes or []:
            self.prefixes.add_prefix(prefix)

    async def _custom_prefixes(self, message: T) -> typing.List[str]:
        if self.prefix_callback is None:
            return []

        return await self.prefix_callback(message)

    async def handle(self, message: T) -> None:
        if message.content is None:
            return

        is_prefixed, prefix, rest = self.prefixes.deprefix(message.content)
        if not is_prefixed:
            custom_prefixes = await self._custom_prefixes(message)
            # a linear scan will more than likely be faster and easier than making a custom trie for every message.
            working_prefixes = [prefix for prefix in custom_prefixes if message.content[:len(prefix)] == prefix]

            if not working_prefixes:
                return

            prefix = max(working_prefixes)
            rest = message.content[len(prefix):]

        has_command, command, args, commands = self.commands.decommand(rest.split(' '))
        if not has_command:
            return

        command_str = ' '.join(command)
        failed: typing.Set[QuackCommand[T]] = set()
        succeeded: typing.Set[QuackCommand[T]] = set()
        errors: typing.MutableMapping[
            QuackCommand[T], typing.Optional[BaseException]
        ] = collections.defaultdict(lambda: None)
        commands = commands.copy()

        while commands:
            message_event = MessageEvent(prefix, command_str, message, errors, self, args)
            pending = {command for command in commands if failed >= command.after_fail}

            if not pending:
                # infinite loop detected!
                self.logger.warning('Infinite loop with `after_fail` detected for: %r', ' '.join(command))
                break

            async with AnyIOExecutor() as tg:
                for command_obj in pending:
                    await tg.spawn(command_obj, message_event, name=command_obj)
                    commands.remove(command_obj)

            for command_obj, err in tg.errs.items():
                errors[command_obj] = err

                if err is not None:
                    if not isinstance(err, ConverterException):
                        # TODO: figure out logger and all its glory
                        # credits to hikari for this one
                        self.logger.error('an exception occurred while handling: %r', message.content, exc_info=err)

                    failed.update({command_obj})
                else:
                    succeeded.update({command_obj})

            for command in commands:
                if command.after_fail.intersection(succeeded):
                    commands.remove(command)

    def _add_cmd_meta(self, cmd_str: str, cmd: QuackCommand[T]) -> None:
        for k, v in cmd.metadata.items():
            self.metadata[cmd_str][k] |= v

            if (self.metadata_lens[cmd_str][k] != -1
                    and cmd.metadata_lens[k] != -1
                    and self.metadata_lens[cmd_str][k] != cmd.metadata_lens[k]):
                # TODO: more descriptive errors.
                raise UserWarning('differing `length_restriction`s.')

            if cmd.metadata_lens[k] != -1:
                self.metadata_lens[cmd_str][k] = cmd.metadata_lens[k]

            if self.metadata_lens[cmd_str][k] != -1 and len(self.metadata[cmd_str][k]) > self.metadata_lens[cmd_str][k]:
                # TODO: more descriptive errors
                raise UserWarning('too much metadata!')

    def _rm_cmd_meta(self, cmd_str: str, cmd: QuackCommand[T]) -> None:
        # TODO: this removes things that others might have
        #   e.g. if a cmd has {2, 3} and another has {3, 4},
        #   removing one will remove 3 from the other...
        raise NotImplementedError('The union of sets CRDT model doesn\'t allow this... Any ideas?')

    def add_command(self, command: str, aliases: typing.Set[str], coro: QuackCommand[T]) -> None:
        self.command_list |= {command}
        self.commands.add_commands(command, coro)

        if self.alias_storage.get(command) is None:
            self.alias_storage[command] = set()

        self.alias_storage[command] |= aliases

        for alias in aliases:
            self.commands.add_commands(alias, coro)

        self._add_cmd_meta(command, coro)

    def remove_command(self, command: str) -> None:
        self.command_list.remove(command)
        self.commands.remove_commands(command)

        for alias in self.alias_storage.pop(command):
            self.commands.remove_commands(alias)

    def remove_specific_command(self, command: str, specific: QuackCommand[T]) -> None:
        # TODO: figure out how to see if this is the last command of it's kind
        #   then call `self.command_list.remove(command)`
        self.commands.remove_specific_command(command.split(' '), specific)

        # TODO: Same as above but for `self.alias_storage`
        for alias in self.alias_storage[command]:
            self.commands.remove_specific_command(alias.split(' '), specific)

    def add_module(self, name: str, module: types.ModuleType) -> None:
        self.modules[name] = module

        for name in dir(module):
            value = getattr(module, name)

            if isinstance(value, QuackCommand):
                self.add_command(value.name, value.aliases, value)
