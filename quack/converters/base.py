import typing

T = typing.TypeVar('T', covariant=True)
ARGS = typing.List[str]


class ConverterException(BaseException):
    pass


@typing.runtime_checkable
class ConverterProto(typing.Protocol[T]):
    async def convert(self, __arguments: ARGS) -> typing.Tuple[ARGS, T]:
        ...
