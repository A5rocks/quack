import typing

from .base import ConverterException, ConverterProto

T = typing.TypeVar('T')
ARGS = typing.List[str]


class OptionalConverter(typing.Generic[T]):
    wraps: ConverterProto[T]
    default: typing.Optional[T]

    def __init__(self, wrapping: ConverterProto[T], default: typing.Optional[T]) -> None:
        self.wraps = wrapping
        self.default = default

    async def convert(self, arguments: ARGS) -> typing.Tuple[ARGS, typing.Optional[T]]:
        if not arguments:
            return [], self.default

        try:
            return await self.wraps.convert(arguments)
        except ConverterException:
            return arguments, self.default
