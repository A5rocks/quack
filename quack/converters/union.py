import typing

from .base import ConverterException, ConverterProto

T = typing.TypeVar('T')
ARGS = typing.List[str]


class UnionConverter(typing.Generic[T]):
    wraps: typing.List[ConverterProto[T]]

    def __init__(self, wrapping: typing.List[ConverterProto[T]]) -> None:
        self.wraps = wrapping

    async def convert(self, arguments: ARGS) -> typing.Tuple[ARGS, T]:
        exceptions = []

        for converter in self.wraps:
            try:
                return await converter.convert(arguments)
            except ConverterException as e:
                exceptions.append(e)

        raise ConverterException('No converters are working.') from Exception(exceptions)
