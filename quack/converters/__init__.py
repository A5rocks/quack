import typing

from ..markers import MinimumList
from .base import ConverterException, ConverterProto
from .greedylist import ListConverter
from .literal import LiteralConverter
from .minlist import MinimumListConverter
from .optional import OptionalConverter
from .union import UnionConverter
from .wrapper import WrapperCallback, WrapperConverter

T = typing.TypeVar('T')
V = typing.TypeVar('V')


async def _int_conv(arg: str) -> int:
    return int(arg)


async def _str_conv(arg: str) -> str:
    return str(arg)

# sorry for the typing.Anys, there's no other way to type this.
default_callbacks: typing.Mapping[
    typing.Type[typing.Any],
    typing.Callable[[str], typing.Awaitable[typing.Any]]
] = {
    int: _int_conv,
    str: _str_conv
}

registered_callbacks: typing.MutableMapping[
    typing.Type[typing.Any],
    ConverterProto[typing.Any]
] = {}


def register_type(typ: typing.Type[T], conv: ConverterProto[T]) -> None:
    registered_callbacks[typ] = conv


def _is_generic(typ: typing.Type[T]) -> bool:
    return hasattr(typ, '__origin__')


def get_converter(
        argument_type: typing.Type[T],
        default: typing.Optional[T],
        next_conv: typing.Optional[ConverterProto[V]]
) -> ConverterProto[T]:
    if isinstance(argument_type, ConverterProto):
        return argument_type

    # so the argument_type is either a generic or a regular class
    generic_type = getattr(argument_type, '__origin__', None)
    generic_args = getattr(argument_type, '__args__', [])

    if generic_type is None:
        # we have to lookup this one
        # and these `typing.cast`s are necessary unfortunately
        registered = typing.cast(ConverterProto[T], registered_callbacks.get(argument_type))
        return registered or WrapperConverter(typing.cast(WrapperCallback[T], default_callbacks[argument_type]))
    elif generic_type is list:
        assert len(generic_args) == 1
        inner_type = generic_args[0]
        inner_converter = get_converter(inner_type, default, next_conv)

        # I'm not sure how to annotate this...
        return typing.cast(ConverterProto[T], ListConverter(inner_converter))
    elif generic_type is MinimumList:
        assert len(generic_args) == 1
        inner_type = generic_args[0]
        inner_converter = get_converter(inner_type, default, next_conv)

        # I'm not sure how to annotate this...
        return typing.cast(ConverterProto[T], MinimumListConverter(inner_converter, next_conv))
    elif generic_type is typing.Union:
        # the second is a common mistake I think...
        is_optional = (type(default) in generic_args) or (None in generic_args)
        convs = filter(lambda cls: cls not in [type(None), None], generic_args)

        union_convs = [get_converter(converter, default, next_conv) for converter in convs]

        converter = UnionConverter(union_convs)

        res_converter: typing.Union[OptionalConverter[T], UnionConverter[T]]

        if is_optional:
            res_converter = OptionalConverter(converter, default)
        else:
            res_converter = converter

        return typing.cast(ConverterProto[T], res_converter)
    elif generic_type is typing.Literal:
        assert len(generic_args) == 1

        argument = generic_args[0]
        assert isinstance(argument, str)

        return typing.cast(ConverterProto[T], LiteralConverter(argument))
    else:
        raise UserWarning(f'there are no possible converter combinations for: {argument_type!r}')


__all__ = ('ConverterProto', 'ConverterException', 'get_converter', 'register_type')
