import typing

from .base import ConverterException, ConverterProto

T = typing.TypeVar('T')
ARGS = typing.List[str]


class ListConverter(typing.Generic[T]):
    wraps: ConverterProto[T]

    def __init__(self, wrapping: ConverterProto[T]) -> None:
        self.wraps = wrapping

    async def convert(self, arguments: ARGS) -> typing.Tuple[ARGS, typing.List[T]]:
        results: typing.List[T] = []

        while arguments:
            try:
                new_arguments, result = await self.wraps.convert(arguments)
            except ConverterException:
                break

            if new_arguments == arguments:
                # oh god.. wtf an infinite loop :/
                # arguably the correct behaviour here
                # is to return unaffected results...
                # List[Optional[int]] will be the same
                # as List[int] :)
                break

            arguments = new_arguments
            results.append(result)

        if not results:
            raise ConverterException('Nothing was converted in ListConverter.')

        return arguments, results
