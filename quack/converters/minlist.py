import typing

from .base import ConverterException, ConverterProto

T = typing.TypeVar('T')
V = typing.TypeVar('V')
ARGS = typing.List[str]


class MinimumListConverter(typing.Generic[T, V]):
    wraps: ConverterProto[T]
    next_conv: typing.Optional[ConverterProto[V]]

    def __init__(
            self,
            wrapping: ConverterProto[T],
            next_conv: typing.Optional[ConverterProto[V]]
    ) -> None:
        self.wraps = wrapping
        self.next_conv = next_conv

    async def convert(self, arguments: ARGS) -> typing.Tuple[ARGS, typing.List[T]]:
        results: typing.List[T] = []

        while arguments:
            try:
                if self.next_conv is not None:
                    await self.next_conv.convert(arguments)
                    return arguments, results
            except ConverterException:
                pass

            try:
                arguments, res = await self.wraps.convert(arguments)
                results.append(res)
            except ConverterException:
                return arguments, results

        return arguments, results
