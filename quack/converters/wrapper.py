import typing

from .base import ConverterException

T = typing.TypeVar('T', covariant=True)
V = typing.TypeVar('V')
ARGS = typing.List[str]


class WrapperCallback(typing.Protocol[T]):
    async def __call__(self, __argument: str) -> T:
        ...


class WrapperConverter(typing.Generic[V]):
    wraps: WrapperCallback[V]

    def __init__(self, wrapping: WrapperCallback[V]) -> None:
        self.wraps = wrapping

    async def convert(self, arguments: ARGS) -> typing.Tuple[ARGS, V]:
        if not arguments:
            raise ConverterException('Ran out of arguments!')

        try:
            return arguments[1:], await self.wraps(arguments[0])
        except ValueError as e:
            raise ConverterException('Bad argument') from e
