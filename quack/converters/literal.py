import typing

from .base import ConverterException

ARGS = typing.List[str]


class LiteralConverter:
    wants: typing.List[str]

    def __init__(self, wants: str) -> None:
        self.wants = wants.split(' ')

    def starts_with(self, args: ARGS) -> bool:
        wants = self.wants

        if len(wants) > len(args):
            return False

        for i, argument in enumerate(wants):
            if not args[i] == argument:
                return False

        return True

    async def convert(self, arguments: ARGS) -> typing.Tuple[ARGS, str]:
        if not arguments:
            raise ConverterException('Nothing passed into literal converter.')

        if not self.starts_with(arguments):
            raise ConverterException('The arguments do not match the literal.')

        return arguments[len(self.wants):], ' '.join(self.wants)
