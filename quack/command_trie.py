# similar to the `prefix_trie.py`, but for nested commands :)

import typing

from .command import QuackCommand, Command
from .event import MessageProto

MESSAGE = typing.List[str]
T = typing.TypeVar('T', bound=MessageProto)


# This can be made not-recursive - is it worth it?
class CommandTrie(typing.Generic[T]):
    # this removes ambiguity by always using the longer command.
    # keep that in mind.
    __slots__ = ('eot', 'branches', 'coros')

    eot: bool
    branches: typing.MutableMapping[str, 'CommandTrie[T]']
    # TODO: rm QuackCommand
    coros: typing.List[typing.Union[Command[T], QuackCommand[T]]]

    def __init__(self) -> None:
        self.eot = False
        self.branches = {}
        self.coros = []

    # TODO: rm this
    def add_commands(self, command: str, coro: QuackCommand[T]) -> None:
        # it's like `load` vs `loads`
        self.add_command(command.split(' '), coro)

    def add_command(self, command: MESSAGE, coro: QuackCommand[T]) -> None:
        if not command:
            self.coros.append(coro)
            self.eot = True
            return

        first_str = command[0].casefold()
        if self.branches.get(first_str, None) is None:
            self.branches[first_str] = CommandTrie()

        self.branches[first_str].add_command(command[1:], coro)

    # TODO: rm this
    def remove_commands(self, command: str) -> None:
        self.remove_command(command.split(' '))

    # TODO: rm this
    def remove_command(self, command: MESSAGE) -> None:
        if not command:
            self.eot = False
            self.coros = []
            return

        first_str = command[0]
        if self.branches.get(first_str, None) is None:
            return

        self.branches[first_str].remove_command(command[1:])

    # TODO: rename this to the above
    def remove_specific_command(self, command: MESSAGE, specific: QuackCommand[T]) -> None:
        if not command:
            self.coros.remove(specific)

            if not self.coros:
                self.eot = False

            return

        first_part = command[0]
        if self.branches.get(first_part, None) is None:
            return

        self.branches[first_part].remove_specific_command(command[1:], specific)

    def decommand(self, message: MESSAGE) -> typing.Tuple[bool, MESSAGE, MESSAGE, typing.List[QuackCommand[T]]]:
        if not message:
            return self.eot, [], message, self.coros

        first_str = message[0]
        first_str_ci = message[0].casefold()
        if self.branches.get(first_str_ci, None) is None:
            return self.eot, [], message, self.coros

        is_command, command, message, coros = self.branches[first_str_ci].decommand(message[1:])
        return is_command or self.eot, [first_str] + command, message, coros or self.coros
