# contains the decorators that make checks and middleware.
import functools
import typing

from .command import Check, MiddlewareCallable, QuackCommand
from .event import MessageEvent, MessageProto

T = typing.TypeVar('T', bound=MessageProto)


def check() -> typing.Callable[[Check],
                                     typing.Callable[..., typing.Callable[[QuackCommand[T]], QuackCommand[T]]]]:
    def wrapped_present(coro: Check) -> typing.Callable[..., typing.Callable[[QuackCommand[T]], QuackCommand[T]]]:
        @functools.wraps(coro)
        def wrapped_decoration(*args: typing.Any, **kwargs: typing.Any
                               ) -> typing.Callable[[QuackCommand[T]], QuackCommand[T]]:
            async def correct_coro(event: MessageEvent[T]) -> bool:
                return await coro(event, *args, **kwargs)

            def wrapped_wrapped_present(command: QuackCommand[T]) -> QuackCommand[T]:
                command.add_check(correct_coro)
                return command

            return wrapped_wrapped_present

        return typing.cast(
            typing.Callable[..., typing.Callable[[QuackCommand[T]], QuackCommand[T]]],
            wrapped_decoration
        )

    return wrapped_present


def middleware() -> typing.Callable[[MiddlewareCallable[T]],
                                     typing.Callable[..., typing.Callable[[QuackCommand[T]], QuackCommand[T]]]]:
    def wrapped_present(coro: MiddlewareCallable[T]
                        ) -> typing.Callable[..., typing.Callable[[QuackCommand[T]], QuackCommand[T]]]:
        @functools.wraps(coro)
        def wrapped_decoration(*args: typing.Any, **kwargs: typing.Any
                               ) -> typing.Callable[[QuackCommand[T]], QuackCommand[T]]:
            async def correct_coro(event: MessageEvent[T]) -> MessageEvent[T]:
                return await coro(event, *args, **kwargs)

            def wrapped_wrapped_present(command: QuackCommand[T]) -> QuackCommand[T]:
                command.add_middleware(correct_coro)
                return command

            return wrapped_wrapped_present

        return typing.cast(
            typing.Callable[..., typing.Callable[[QuackCommand[T]], QuackCommand[T]]],
            wrapped_decoration
        )

    return wrapped_present
