import collections
import types
import typing

import anyio

if typing.TYPE_CHECKING:
    from .command import QuackCommand
else:
    QuackCommand = typing.Union

T = typing.TypeVar('T')
# TODO: use hashes for space reasons?
IDENTIFIER = QuackCommand[typing.Any]


# adapted from https://gist.github.com/smurfix/d528056259496d1b7ada1c20b9aee6fb
# TODO: find some sort of stubs / make them myself?
class AnyIOExecutor(anyio.TaskGroup):  # type: ignore
    __slots__ = ('_tg', 'errs')

    _tg: typing.Optional[anyio.TaskGroup]  # type: ignore
    errs: typing.MutableMapping[IDENTIFIER, typing.Optional[BaseException]]

    def __init__(self) -> None:
        self.errs = collections.defaultdict(lambda: None)
        self._tg = None

    async def __aenter__(self) -> 'AnyIOExecutor':
        self._tg = anyio.create_task_group()
        await self._tg.__aenter__()

        return self

    async def __aexit__(
            self,
            exc_type: typing.Optional[typing.Type[BaseException]],
            exc: typing.Optional[BaseException],
            tb: typing.Optional[types.TracebackType]
    ) -> typing.Optional[bool]:
        if self._tg is None:
            # I'm not sure
            return None

        try:
            return typing.cast(typing.Optional[bool], await self._tg.__aexit__(exc_type, exc, tb))
        finally:
            del self._tg

    async def _run_task(
            self,
            identifier: IDENTIFIER,
            proc: QuackCommand[typing.Any],
            *args: typing.Any
    ) -> None:
        try:
            await proc(*args)
            self.errs[identifier] = None
        except BaseException as e:
            self.errs[identifier] = e

    async def spawn(
            self,
            proc: QuackCommand[typing.Any],
            *args: typing.Any,
            name: typing.Optional[IDENTIFIER] = None,
            **kwargs: typing.Any
    ) -> None:
        # this is just here to make sure we have the same interface. optimization should get rid of this
        assert name is not None
        if self._tg is None:
            return

        await self._tg.spawn(self._run_task, name, proc, *args)


class AnyIOResultor(anyio.TaskGroup, typing.Generic[T]):  # type: ignore
    __slots__ = ('_tg', 'results')

    _tg: typing.Optional[anyio.TaskGroup]  # type: ignore
    results: typing.List[typing.Union[T, BaseException]]

    def __init__(self) -> None:
        self._tg = None
        self.results = []

    async def __aenter__(self) -> 'AnyIOResultor[T]':
        self._tg = anyio.create_task_group()
        await self._tg.__aenter__()

        return self

    async def __aexit__(
            self,
            exc_type: typing.Optional[typing.Type[BaseException]],
            exc: typing.Optional[BaseException],
            tb: typing.Optional[types.TracebackType]
    ) -> typing.Optional[bool]:
        if self._tg is None:
            # I'm not sure
            return None

        try:
            return typing.cast(typing.Optional[bool], await self._tg.__aexit__(exc_type, exc, tb))
        finally:
            del self._tg

    async def _run_task(
            self,
            proc: typing.Callable[..., typing.Awaitable[T]],
            *args: typing.Any,
            **kwargs: typing.Any
    ) -> None:
        try:
            self.results.append(await proc(*args, **kwargs))
        except BaseException as e:
            self.results.append(e)

    async def spawn(
            self,
            proc: typing.Callable[..., typing.Awaitable[T]],
            *args: typing.Any) -> None:
        if self._tg is None:
            return

        await self._tg.spawn(self._run_task, proc, *args)
