# A bad implementation of a trie.
import typing


# This can be made not-recursive - is it worth it?
class PrefixTrie:
    # this removes ambiguity by always using the longer prefix.
    # keep that in mind.
    __slots__ = ('eot', 'branches')

    eot: bool
    branches: typing.MutableMapping[str, 'PrefixTrie']

    def __init__(self) -> None:
        self.eot = False
        self.branches = {}

    def add_prefix(self, prefix: str) -> None:
        if not prefix:
            self.eot = True
            return

        first_char = prefix[0]
        if self.branches.get(first_char, None) is None:
            self.branches[first_char] = PrefixTrie()

        self.branches[first_char].add_prefix(prefix[1:])
        return

    def remove_prefix(self, prefix: str) -> None:
        if not prefix:
            self.eot = False
            return

        first_char = prefix[0]
        if self.branches.get(first_char, None) is None:
            return

        self.branches[first_char].remove_prefix(prefix[1:])

    def deprefix(self, message: str) -> typing.Tuple[bool, str, str]:
        if not message:
            return self.eot, message, ''

        first_char = message[0]
        if self.branches.get(first_char, None) is None:
            return self.eot, '', message

        prefixed, prefix, message = self.branches[first_char].deprefix(message[1:])
        return prefixed or self.eot, first_char + prefix, message
