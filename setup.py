# type: ignore
from setuptools import setup

setup(
    name='quack',
    version='0.2.0-dev',
    author='A5rocks',
    author_email='5684371-A5rocks@users.noreply.gitlab.com',
    package_data={'quack': ['py.typed']},
    packages=['quack', 'quack.converters', 'quack.contrib.hikari'],
    license='BSD-3-Clause',
    python_requires='>=3.8.0,<3.10',
    description='A honking good command handler.',
    install_requires=[
        'anyio~=1.4.0'
    ],
    zip_safe=False
)
